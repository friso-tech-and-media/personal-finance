    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->timestamps();

            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('categories');

            $table->string('name');

        });
        
        $this->seedCategories();
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
    
    public function seedCategories(){

        $voedsel = new \App\Category(['id' => 2,'name' => 'Voedsel']);
        $voedsel->save();

        $junkfood = new \App\Category(['name' => 'Junkfood', 'parent_id' => $voedsel->id ]);
        $junkfood->save();

        $item = new \App\Category(['name' => 'Coffee', 'parent_id' => $voedsel->id ]);
        $item->save();

        $item = new \App\Category(['name' => 'Kantine', 'parent_id' => $voedsel->id ]);
        $item->save();

        $pizza    = new \App\Category(['name' => 'pizza', 'parent_id' => $junkfood->id]);
        $pizza->save();        
    }
}

