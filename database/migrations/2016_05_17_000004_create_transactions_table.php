<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');

            $table->string('account_id');
            $table->string('contra_name');
            $table->string('contra_account');

            $table->string('type');
            $table->integer('amount')->signed(); 

            // Date is represented with an int yyyymmdd       
            // We want a Unix Timestamp
            $table->integer('date')->unsigned();
 
            $table->string('description');
            $table->unique(['user_id','date','amount','description']);

            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
