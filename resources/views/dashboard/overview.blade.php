@extends('layouts.neon')

@section('content')
			<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary">
					
					<div class="panel-heading">
						<div class="panel-title">Inkomsten / Uitgaven</div>
					</div>
					
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="50%">
									<strong>Area Chart</strong>
									<br />
									<div id="chart10" style="height: 300px"></div>
								</td>
								<td>
									<strong>Deze Maand</strong>
		<br />
									<div id="food" style="height: 300px"></div
								</td>
							</tr>
						</tbody>
					</table>
					
				</div>
			
			</div>
		</div>

@endsection


@section('scripts')

	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="/theme/neon/assets/js/rickshaw/rickshaw.min.css">

	<!-- Imported scripts on this page -->
	<script src="/theme/neon/assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="/theme/neon/assets/js/raphael-min.js"></script>
	<script src="/theme/neon/assets/js/morris.min.js"></script>
	<script src="/theme/neon/assets/js/jquery.peity.min.js"></script>
	<script src="--/theme/neon/assets/js/neon-charts.js"></script>
	<script src="/theme/neon/assets/js/jquery.sparkline.min.js"></script>
	


<script>

var financialData = {!! $t !!};
Morris.Bar({
  element: 'chart10',
  data: financialData,
  xkey: 'y',
  ykeys: ['total'],
  labels: ['total', 'Credits'],
  barColors: ['red', 'green'],
  fillOpacity: 0.5
});

var foodData = {!! json_encode($onFood) !!};
Morris.Line({
  element: 'food',
  data: foodData,
  xkey: 'month',
  ykeys: ['total'],
  labels: ['total', 'Credits'],
  fillOpacity: 0.5
});


</script>
	
@endsection