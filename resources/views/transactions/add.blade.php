@extends('layouts.neon')

@section('content')
<div class="col-md-12">
	<form action="{{ Request::url() }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="file" name="csv"  accept=".csv"> <br />
		<select name="bank">
			<option value="ING">ING</option>
			<option value="RABOBANK">RaboBank</option>
		</select>
		<input type="submit" value="Uploaden" class="btn btn-primary">
	</form>
</div>
@endsection