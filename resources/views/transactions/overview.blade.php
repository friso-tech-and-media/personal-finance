@extends('layouts.neon') 
@section('content')
<table class="table table-striped">
	<thead>
		<th>Tegenrekening</th>
		<th>Naam</th>
		<th>Mededeling</th>
		<th>Bedrag</th>

		</tr>
	</thead>
	<tbody>
		@foreach ($transactions as $transaction)
		<tr>
			<td>{{ $transaction->contra_account }}</td>
			<td>{{ $transaction->contra_name }}</td>
			<td>{{ $transaction->description }}</td>
			<td>{{ $transaction->amount /100 }}</td>
		</tr>
		@endforeach
	</tbody>
</table>


@endsection