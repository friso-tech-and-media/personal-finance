</div>
	<!-- Bottom scripts (common) -->
	<script src="/theme/neon/assets//js/gsap/main-gsap.js"></script>
	<script src="/theme/neon/assets//js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="/theme/neon/assets//js/bootstrap.js"></script>
	<script src="/theme/neon/assets//js/joinable.js"></script>
	<script src="/theme/neon/assets//js/resizeable.js"></script>
	<script src="/theme/neon/assets//js/neon-api.js"></script>
	<script src="/theme/neon/assets//js/rickshaw/vendor/d3.v3.js"></script>

	<!-- Extra Scripts-->
	@section('scripts')
	
	@show

</body>
</html>