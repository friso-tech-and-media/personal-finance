<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
    /**
     * Show the main dashboard
     *
     * @return \Illuminate\Http\Response
     */
     public function overview(){
		 $t     = new \App\Transaction();

         $view  = view('dashboard.overview');
         $view->with([
             't'            => json_encode( $t->getDebits() ),
             'categories'   => $t->categories(),
             'onFood'       => $t->spentOnFood()
         ]);

         return $view;
     }   
}
