<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Lib\TransactionImport\TransactionImport;
use Auth;
use App\Lib\Helper;

class TransactionController extends Controller
{
    /**
     * Show the main dashboard
     *
     * @return \Illuminate\Http\Response
     */
     public function overview(){
         
        $transactions = Auth::user()->transactions()->get() ;
         return view('transactions.overview')->with([
             'transactions' => $transactions
         ]);
     }   

    /**
     * Show the interface, allowing a use to add transactions
     *
     * @return \Illuminate\Http\Response
     */
     public function add(){
         return view('transactions.add');
     }   
     
    /**
     * Show the interface, allowing a use to add transactions
     *
     * @param \Illuminiate\Http\Request $request The request object
     * @return \Illuminate\Http\Response
     */
    public function processAdd(Request $request){
        if (!$request->hasFile('csv') )
            return 'error';

        $file =  $request->file('csv') ;          
        $transaction = TransactionImport::import($file, $request->input('bank'));
        
        return $this->add();
    }     
}
