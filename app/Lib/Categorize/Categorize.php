<?php
namespace App\Lib\Categorize;

use Illuminate\Database\Eloquent\Collection as Collection;
/**
 *
 */
class Categorize{
	protected $inout 	= array();
	protected $methods  = array();
	
	const CAT_FOOD 	 		= 'Voedsel';
	const CAT_404 	 		= 'Overigen';
	const CAT_JUNKFOOD 		= 'Junkfood';
	const CAT_SUPPLEMENT 	= 'Supplement';
	const CAT_BENZINE 		= 'Bezine';
	
	/**
	 *
	 *
	 */
	public function categorize(Collection $transactions){
		$all_cat = \App\Category::all()->keyBy('name');
		
		foreach ($transactions as $t){
			$cat_name	= $this->guessCat( $t );

			if ( isset($all_cat[ $cat_name ] ) )
				$t->category_id = $all_cat[ $cat_name ]->id;
		}

		return $transactions;
	}

	public function guessCat($t){
		foreach ($this->methods as $name) {
			$omschrijving 	= 	$t->description;
			$mededeling		= 	$t->contra_name;

			$code 			= call_user_func_array( array($this, $name), array($omschrijving, $mededeling) );	
			if ($code !== null)
				break;
		}
		
		return $code == null ? self::CAT_404 : $code; 		
	}

	function __construct(){
		$arr = (new \ReflectionClass(__CLASS__))->getMethods()  ;
		
		foreach ($arr as $key => $value) 
			if (strpos($value->name, 'is')===0)
				$this->methods[] = $value->name;
	}
		
	/* Check Categories
	------------------------------------------ */
	public function isFood($omschrijving, $mededeling){
		if (preg_match('/(Albert Heijn|Jumbo|Aldi|muscle)/i', $omschrijving) || preg_match('/(Albert Heijn|Jumbo|Aldi|muscle)/i', $mededeling))
			return self::CAT_FOOD;	

		return null;
	}
	
	public function isJunkFood($omschrijving, $mededeling){		
		if (preg_match('/(Dominos|pizza)/i', $omschrijving) || preg_match('/(Dominos|Pizza)/i', $mededeling))
			return self::CAT_JUNKFOOD;
			
		return null;
	}
	
	public function isSupplement($omschrijving, $mededeling){
		if (preg_match('/(body.*?Fit)/i', $omschrijving) || preg_match('/(body.*?Fit)/i', $mededeling))
			return self::CAT_SUPPLEMENT;
			
		return null;		
	}

		public function isCoffee($omschrijving, $mededeling){
		if (preg_match('/(Douwe Egberts|Starbucks)/i', $omschrijving) || preg_match('/(Douwe Egberts|Starbucks)/i', $mededeling))
			return 'Coffee';
			
		return null;		
	}
	
	public function isKatine($omschrijving, $mededeling){
		if (preg_match('/(Sodexo)/i', $omschrijving) || preg_match('/(Sodex)/i', $mededeling))
			return 'Kantine';
			
		return null;		
	}

	public function isBezine($omschrijving, $mededeling){
	if (preg_match('/(SELF SERVICE)/i', $omschrijving) || preg_match('/(SELF SERVICE)/i', $mededeling))
			return self::CAT_BENZINE;
			
		return null;		
	}
	
	public function get(){
		return $this->inout;
	}
}
