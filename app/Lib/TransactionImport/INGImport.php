<?php
namespace App\Lib\TransactionImport;

use Illuminate\Http\UploadedFile as UploadedFile;
use Illuminate\Database\Eloquent\Collection as Collection;

use App\Transaction as Transaction;

use Auth as Auth;
use DB;

class INGImport implements TransactionImportInterface
{

	public function process(UploadedFile $file){
		$transactions = new Collection();

		$file 	= fopen($file->getRealPath(), 'r');
		$header =  fgetcsv($file);

		while (($line = fgetcsv($file)) !== FALSE) {
			$t = (object)array_combine($header, $line);
			$transaction 	= new Transaction();
			$transaction->user_id = Auth::id();

			$transaction->amount = strcasecmp($t->{'Af Bij'},'af') === 0 ? '-' . str_replace(',', '.', $t->{'Bedrag (EUR)'}) : str_replace(',', '.', $t->{'Bedrag (EUR)'});
			// Respresented as an integer in cent (floats aren't accurate for monies)
			$transaction->amount = $transaction->amount * 100;

			$transaction->description = $t->{'Mededelingen'};
			$transaction->contra_account= $t->{'Tegenrekening'};
			$transaction->contra_name= $t->{'Naam / Omschrijving'};

			$date = \DateTime::createFromFormat('Ymd', $t->{'Datum'} ) ->getTimestamp();
			$transaction->date = $date;

			$transactions->push($transaction);
		}

		fclose($file);

		return $transactions;
	}


}
