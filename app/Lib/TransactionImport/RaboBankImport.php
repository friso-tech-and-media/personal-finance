<?php
namespace App\Lib\TransactionImport;

use Illuminate\Http\UploadedFile as UploadedFile;
use Illuminate\Database\Eloquent\Collection as Collection;
use Auth as Auth;
use App\Transaction as Transaction;

class RaboBankImport implements TransactionImportInterface{

	public function process(UploadedFile $file){
		$transactions = new Collection();

		$file 	= fopen($file->getRealPath(), 'r');
		$header =  array(
			'Rekening',
			'Munt',
			'Datum',
			'Af bij',
			'Bedrag',
			'Naar_rek',
			'Naar_naam',
			'Boek_datum',
			'Boekcode',
			'Filler',
			'Omschrijving'	
			
		);

		while (($line = fgetcsv($file)) !== FALSE) {
			$omschrijving =  implode("\r\n", array_slice( $line, 10,5));
			array_splice($line,10, count($line),$omschrijving );
			$t = (object)array_combine($header, $line);
		

			$transaction 	= new Transaction();
			$transaction->user_id = Auth::id();

			$transaction->amount = strcasecmp($t->{'Af bij'},'d') === 0 ? '-' . str_replace(',', '.', $t->{'Bedrag'}) : str_replace(',', '.', $t->{'Bedrag'});
			// Respresented as an integer in cent (floats aren't accurate for monies)
			$transaction->amount = $transaction->amount * 100;

			$transaction->description 	= $omschrijving;
			$transaction->contra_account = $t->{'Naar_rek'};
			$transaction->contra_name= $t->{'Naar_naam'};

			$date = \DateTime::createFromFormat('Ymd', $t->{'Datum'} ) ->getTimestamp();
			$transaction->date = $date;

			$transactions->push($transaction);

			
		}

		fclose($file);

	
	return $transactions;
}

}
