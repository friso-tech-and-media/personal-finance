<?php
namespace App\Lib\TransactionImport;

use Illuminate\Http\UploadedFile as UploadedFile;

interface TransactionImportInterface{

	public function process( UploadedFile $file );
	
	
}


