<?php

namespace App\Lib\TransactionImport;

use Illuminate\Http\UploadedFile as UploadedFile;
use App\Lib\Categorize\Categorize as Categorize;
use Illuminate\Database\QueryException;

use App\Lib\Transaction\Transaction as Transaction;
use DB as DB;
class TransactionImport
{
	/**
	 * Bank identifiers
	 */
	const BANK_NOT_FOUND	= null;
	const BANK_ING 			= 'ING';
	const BANK_RABOBANK 	= 'RaboBank';

	/**
	 *
	 */	
	public static function import(UploadedFile $file , $bank){
		$bank =  constant('self::BANK_' . $bank);
		if ($bank==null)
			die('Invalid bank');

		self::process( $file, $bank);
	}

	private static function process( $file, $bank ){
		$bank 			= __NAMESPACE__ . '\\' .  $bank. 'Import';
		$bank 			= new $bank();
		
		$transactions 	= $bank->process( $file ) ;		
		$cat 			= new Categorize();
		$transactions 	= $cat->categorize($transactions);
 
	 	DB::transaction(function () use ($transactions) {
			foreach ($transactions as $t) 
				self::insertTransaction( $t );
		 });
	}	

	private static function insertTransaction( $t ){
		try{
			$t->save();

		} catch(QueryException $e){

		}
	}	

	private static function guessBank( $file ){
		$bank = self::BANK_NOT_FOUND;
		return $bank;
	}
	
}
